package com.mwong56.blogger.views;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mwong56.blogger.models.Post;

public class PostView {
  public Post post_;
  public TextView titleView_;
  public TextView textView_;
  public ImageView imageView_;
  public LinearLayout postDetails_;
}
