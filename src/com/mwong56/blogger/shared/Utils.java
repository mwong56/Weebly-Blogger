package com.mwong56.blogger.shared;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;

public class Utils {

  public static final String ACTION = "ACTION";
  public static final String ID = "ID";
  private static String TAG = "Utils";

  public enum Status {
    NEW, EDIT
  }

  public Utils() {
    return;
  }

  public static String getUniqueImageFilename() {
    return "img_" + System.currentTimeMillis() + ".jpg";
  }

  public static String getUniqueVideoFilename() {
    return "video_" + System.currentTimeMillis() + ".mp4";
  }

  public static String getFilePath(Context context, Uri uri) {
    String filePath = "";
    String scheme = uri.getScheme();
    ContentResolver contentResolver = context.getContentResolver();

    // If we are sent file://something or
    // content://org.openintents.filemanager/mimetype/something...
    if (scheme.equals("file")
        || (scheme.equals("content") && uri.getEncodedAuthority().equals(
            "org.openintents.filemanager"))) {
      // Get the path
      filePath = uri.getPath();

      // Trim the path if necessary
      // openintents filemanager returns
      // content://org.openintents.filemanager/mimetype//mnt/sdcard/xxxx.mp4
      if (filePath.startsWith("/mimetype/")) {
        String trimmedFilePath = filePath.substring("/mimetype/".length());
        filePath = trimmedFilePath.substring(trimmedFilePath.indexOf("/"));
      }
    } else if (scheme.equals("content")) {
      // If we are given another content:// URI, look it up in the media
      // provider
      // videoId = Long.valueOf(selectedVideoUri.getLastPathSegment());
      filePath = Utils.getFilePathFromContentUri(uri, contentResolver);
    } else {
      Log.d(TAG, "Failed to load URI " + uri.toString());
    }
    return filePath;
  }

  /**
   * Gets the corresponding path to a file from the given content:// URI
   * 
   * @param selectedVideoUri
   *          The content:// URI to find the file path from
   * @param contentResolver
   *          The content resolver to use to perform the query.
   * @return the file path as a string
   */
  public static String getFilePathFromContentUri(Uri selectedVideoUri,
      ContentResolver contentResolver) {
    String filePath;
    String[] filePathColumn = { MediaColumns.DATA };

    Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
    cursor.moveToFirst();

    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
    filePath = cursor.getString(columnIndex);
    cursor.close();
    return filePath;
  }

  public static String getGoogleMapUrl(double lati, double longi) {
    String URL = "http://maps.google.com/maps/api/staticmap?center=" + lati + "," + longi
        + "&zoom=15&size=200x200&sensor=false";

    return URL;
  }

}
