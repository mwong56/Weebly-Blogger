package com.mwong56.blogger.sqlite;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mwong56.blogger.models.Element;
import com.mwong56.blogger.models.Post;
import com.mwong56.blogger.shared.ElementsCallback;
import com.mwong56.blogger.shared.PostsCallback;

public class SQLDataSource {
  private static SQLDataSource instance = null;

  private SQLiteDatabase database;
  private SQLiteHelper dbHelper;
  private String[] allPostColumns = { SQLiteHelper.POSTS_COLUMN_ID,
      SQLiteHelper.POSTS_COLUMN_NUMDAY, SQLiteHelper.POSTS_COLUMN_NAMEDAY,
      SQLiteHelper.POSTS_COLUMN_MONTHYEAR, SQLiteHelper.POSTS_COLUMN_TIME,
      SQLiteHelper.POSTS_COLUMN_TITLE, SQLiteHelper.POSTS_COLUMN_POST,
      SQLiteHelper.POSTS_COLUMN_IMAGE };
  private String[] allElementColumns = { SQLiteHelper.ELEMENTS_COLUMN_ID,
      SQLiteHelper.ELEMENTS_COLUMN_POST_ID, SQLiteHelper.ELEMENTS_COLUMN_TYPE,
      SQLiteHelper.ELEMENTS_COLUMN_CONTENT, SQLiteHelper.ELEMENTS_COLUMN_ORDER };

  private final String TAG = this.getClass().getSimpleName();

  public static SQLDataSource getInstance(Context context) {
    if (instance == null) {
      instance = new SQLDataSource(context);
    }

    return instance;
  }

  public SQLDataSource(Context context) {
    dbHelper = new SQLiteHelper(context);
    this.open();
  }

  private void open() throws SQLException {
    database = dbHelper.getWritableDatabase();
  }

  public void close() {
    dbHelper.close();
  }

  public Post createPost(String numday, String nameday, String monthyear, String time,
      String title, String post, String image) {
    ContentValues values = new ContentValues();
    values.put(SQLiteHelper.POSTS_COLUMN_NUMDAY, numday);
    values.put(SQLiteHelper.POSTS_COLUMN_NAMEDAY, nameday);
    values.put(SQLiteHelper.POSTS_COLUMN_MONTHYEAR, monthyear);
    values.put(SQLiteHelper.POSTS_COLUMN_TIME, time);
    values.put(SQLiteHelper.POSTS_COLUMN_TITLE, title);
    values.put(SQLiteHelper.POSTS_COLUMN_POST, post);
    values.put(SQLiteHelper.POSTS_COLUMN_IMAGE, image);
    long insertId = database.insert(SQLiteHelper.TABLE_POSTS, null, values);
    Cursor cursor = database.query(SQLiteHelper.TABLE_POSTS, allPostColumns,
        SQLiteHelper.POSTS_COLUMN_ID + " = " + insertId, null, null, null, null);
    cursor.moveToFirst();
    Post postToReturn = Post.cursorToPost(cursor);
    cursor.close();
    return postToReturn;
  }

  public void readPost(Long id, final PostsCallback callback) {
    Cursor cursor = database.query(SQLiteHelper.TABLE_POSTS, allPostColumns,
        SQLiteHelper.POSTS_COLUMN_ID + " = " + id, null, null, null, null);
    cursor.moveToFirst();
    final Post postToReturn = Post.cursorToPost(cursor);
    cursor.close();
    getAllElements(postToReturn.getId(), new ElementsCallback() {

      @Override
      public void callback(List<Element> elements) {
        postToReturn.setElements(elements);
        callback.callback(postToReturn);
      }
    });
  }

  public void updatePost(Post post) {
    ContentValues values = new ContentValues();
    values.put(SQLiteHelper.POSTS_COLUMN_TITLE, post.getTitle());
    values.put(SQLiteHelper.POSTS_COLUMN_POST, post.getPost());
    values.put(SQLiteHelper.POSTS_COLUMN_IMAGE, post.getImage());
    database.update(SQLiteHelper.TABLE_POSTS, values,
        SQLiteHelper.POSTS_COLUMN_ID + " = " + post.getId(), null);
  }

  public void deletePost(Post post) {
    Log.i(TAG, "Deleting post with id: " + post.getId());
    database.delete(SQLiteHelper.TABLE_POSTS, SQLiteHelper.POSTS_COLUMN_ID + " = " + post.getId(),
        null);
    database.delete(SQLiteHelper.TABLE_ELEMENTS, SQLiteHelper.ELEMENTS_COLUMN_POST_ID + " = "
        + post.getId(), null);
  }

  public void getAllPosts(PostsCallback callback) {
    List<Post> posts = new ArrayList<Post>();
    Cursor cursor = database.query(SQLiteHelper.TABLE_POSTS, allPostColumns, null, null, null,
        null, null, null);

    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Post post = Post.cursorToPost(cursor);
      posts.add(post);
      cursor.moveToNext();
    }

    cursor.close();
    callback.callback(posts);
  }

  public Element createElement(long id, String type, String content, int order) {
    ContentValues values = new ContentValues();
    values.put(SQLiteHelper.ELEMENTS_COLUMN_POST_ID, id);
    values.put(SQLiteHelper.ELEMENTS_COLUMN_TYPE, type);
    values.put(SQLiteHelper.ELEMENTS_COLUMN_CONTENT, content);
    values.put(SQLiteHelper.ELEMENTS_COLUMN_ORDER, order);
    long insertId = database.insert(SQLiteHelper.TABLE_ELEMENTS, null, values);
    Cursor cursor = database.query(SQLiteHelper.TABLE_ELEMENTS, allElementColumns,
        SQLiteHelper.ELEMENTS_COLUMN_ID + " = " + insertId, null, null, null, null);
    cursor.moveToFirst();
    Element elementToReturn = Element.cursorToElement(cursor);
    cursor.close();
    return elementToReturn;
  }

  public void updateElement(Element element) {
    ContentValues values = new ContentValues();
    values.put(SQLiteHelper.ELEMENTS_COLUMN_TYPE, element.getType());
    values.put(SQLiteHelper.ELEMENTS_COLUMN_CONTENT, element.getContent());
    values.put(SQLiteHelper.ELEMENTS_COLUMN_ORDER, element.getOrder());
    database.update(SQLiteHelper.TABLE_ELEMENTS, values, SQLiteHelper.POSTS_COLUMN_ID + " = "
        + element.getId(), null);
  }

  public void deleteElement(Element element) {
    int rows = database.delete(SQLiteHelper.TABLE_ELEMENTS, SQLiteHelper.ELEMENTS_COLUMN_ID + " = "
        + element.getId(), null);
    Log.i(TAG, "Deleting element with id: " + element.getId() + " Rows: " + rows);
  }

  public void getAllElements(Long postid, ElementsCallback callback) {
    List<Element> elements = new ArrayList<Element>();
    Cursor cursor = database.query(SQLiteHelper.TABLE_ELEMENTS, allElementColumns,
        SQLiteHelper.ELEMENTS_COLUMN_POST_ID + " = " + postid, null, null, null, null, null);

    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      Element element = Element.cursorToElement(cursor);
      elements.add(element);
      cursor.moveToNext();
    }

    cursor.close();
    if (callback != null) {
      callback.callback(elements);
    }
  }

}
