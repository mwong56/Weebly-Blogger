package com.mwong56.blogger.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mwong56.blogger.R;
import com.mwong56.blogger.models.Post;
import com.mwong56.blogger.views.PostView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PostAdapter extends BaseAdapter {

  private Context context_;
  private List<Post> postList_;

  public PostAdapter(Context context, List<Post> postList) {
    this.context_ = context;
    this.postList_ = postList;
  }

  @Override
  public int getCount() {
    return postList_.size();
  }

  @Override
  public Object getItem(int position) {
    return postList_.get(position);
  }

  @Override
  public long getItemId(int position) {
    return postList_.indexOf(postList_.get(position));
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View view = convertView;
    final PostView holder;

    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context_
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      view = inflater.inflate(R.layout.item_post, parent, false);
      holder = new PostView();
      holder.titleView_ = (TextView) view.findViewById(R.id.postTitle);
      holder.textView_ = (TextView) view.findViewById(R.id.postText);
      holder.imageView_ = (ImageView) view.findViewById(R.id.postImage);
      holder.postDetails_ = (LinearLayout) view.findViewById(R.id.postDetails);
      view.setTag(holder);
    } else {
      holder = (PostView) view.getTag();
    }

    Post post = postList_.get(position);
    holder.post_ = post;
    if (post.getTitle() != null) {
      holder.titleView_.setText(post.getTitle());
    } else {
      holder.titleView_.setText("Post #" + post.getId());
    }

    if (post.getPost() != null) {
      holder.textView_.setText(post.getPost());
    } else {
      holder.textView_.setText("");
    }

    if (post.getImage() != null) {
      holder.postDetails_.setLayoutParams(new LinearLayout.LayoutParams(0,
          LayoutParams.MATCH_PARENT, 0.6f));
      holder.imageView_.setVisibility(View.VISIBLE);
      ImageLoader.getInstance().displayImage(post.getImage(), holder.imageView_);
    } else {
      holder.postDetails_.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
          LayoutParams.MATCH_PARENT));
      holder.imageView_.setVisibility(View.GONE);
    }

    return view;
  }
}