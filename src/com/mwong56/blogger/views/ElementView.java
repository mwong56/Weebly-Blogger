package com.mwong56.blogger.views;

import android.widget.ImageView;
import android.widget.TextView;

import com.mwong56.blogger.models.Element;

public class ElementView {
  public Element element_;
  public TextView text_;
  public ImageView image_;
}
