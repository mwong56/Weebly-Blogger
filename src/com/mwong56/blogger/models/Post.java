package com.mwong56.blogger.models;

import java.util.List;

import android.database.Cursor;

public class Post {

  private long id_;
  private String numday_;
  private String nameday_;
  private String monthyear_;
  private String time_;
  private String title_;
  private String post_;
  private String image_;
  private List<Element> elements_;

  public Post() {
    return;
  }

  public static Post cursorToPost(Cursor cursor) {
    Post post = new Post();
    post.setId(cursor.getLong(0));
    post.setNumday(cursor.getString(1));
    post.setNameday(cursor.getString(2));
    post.setMonthyear(cursor.getString(3));
    post.setTime(cursor.getString(4));
    post.setTitle(cursor.getString(5));
    post.setPost(cursor.getString(6));
    post.setImage(cursor.getString(7));
    return post;
  }

  public long getId() {
    return id_;
  }

  public void setId(long id) {
    this.id_ = id;
  }

  public String getNumday() {
    return numday_;
  }

  public void setNumday(String numday) {
    this.numday_ = numday;
  }

  public String getNameday() {
    return nameday_;
  }

  public void setNameday(String nameday) {
    this.nameday_ = nameday;
  }

  public String getMonthyear() {
    return monthyear_;
  }

  public void setMonthyear(String monthyear) {
    this.monthyear_ = monthyear;
  }

  public String getTime() {
    return time_;
  }

  public void setTime(String time) {
    this.time_ = time;
  }

  public String getTitle() {
    return title_;
  }

  public void setTitle(String title) {
    this.title_ = title;
  }

  public String getPost() {
    return post_;
  }

  public void setPost(String post) {
    this.post_ = post;
  }

  public String getImage() {
    return image_;
  }

  public void setImage(String image) {
    this.image_ = image;
  }

  public List<Element> getElements() {
    return elements_;
  }

  public void setElements(List<Element> elements) {
    this.elements_ = elements;
  }

}
