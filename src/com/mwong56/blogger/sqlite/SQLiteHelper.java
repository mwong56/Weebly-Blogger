package com.mwong56.blogger.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*
 * Creates and upgrades the database.
 * On upgrade, drops the table and recreates the table.
 */
public class SQLiteHelper extends SQLiteOpenHelper {
  public static final String TABLE_POSTS = "posts";
  public static final String POSTS_COLUMN_ID = "_id";
  public static final String POSTS_COLUMN_NUMDAY = "numday";
  public static final String POSTS_COLUMN_NAMEDAY = "nameday";
  public static final String POSTS_COLUMN_MONTHYEAR = "monthyear";
  public static final String POSTS_COLUMN_TIME = "time";
  public static final String POSTS_COLUMN_TITLE = "title";
  public static final String POSTS_COLUMN_POST = "post";
  public static final String POSTS_COLUMN_IMAGE = "image";

  public static final String TABLE_ELEMENTS = "elements";
  public static final String ELEMENTS_COLUMN_ID = "_id";
  public static final String ELEMENTS_COLUMN_POST_ID = "postid";
  public static final String ELEMENTS_COLUMN_TYPE = "type";
  public static final String ELEMENTS_COLUMN_CONTENT = "content";
  public static final String ELEMENTS_COLUMN_ORDER = "elementorder";

  private static final String DATABASE_NAME = "blog.db";
  private static final int DATABASE_VERSION = 2;

  private static final String DATABASE_CREATE_TABLE_POSTS = "create table " + TABLE_POSTS + "("
      + POSTS_COLUMN_ID + " integer primary key autoincrement, " + POSTS_COLUMN_NUMDAY + " text, "
      + POSTS_COLUMN_NAMEDAY + " text, " + POSTS_COLUMN_MONTHYEAR + " text, " + POSTS_COLUMN_TIME
      + " text, " + POSTS_COLUMN_TITLE + " text, " + POSTS_COLUMN_POST + " text, "
      + POSTS_COLUMN_IMAGE + " text);";

  private static final String DATABASE_CREATE_TABLE_ELEMENTS = "create table " + TABLE_ELEMENTS
      + "(" + ELEMENTS_COLUMN_ID + " integer primary key autoincrement, " + ELEMENTS_COLUMN_POST_ID
      + " integer, " + ELEMENTS_COLUMN_TYPE + " text, " + ELEMENTS_COLUMN_CONTENT + " text, "
      + ELEMENTS_COLUMN_ORDER + " integer);";

  public SQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(DATABASE_CREATE_TABLE_POSTS);
    db.execSQL(DATABASE_CREATE_TABLE_ELEMENTS);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.w(SQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to "
        + newVersion + ", which will destroy all old data");
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTS);
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_ELEMENTS);
    onCreate(db);
  }

}
