package com.mwong56.blogger.shared;

import java.util.List;

import com.mwong56.blogger.models.Element;

public interface ElementsCallback {
  public void callback(List<Element> elements);
}
