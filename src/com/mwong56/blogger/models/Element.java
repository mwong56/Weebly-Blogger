package com.mwong56.blogger.models;

import android.database.Cursor;

public class Element implements Comparable<Element> {

  private long id_;
  private long postid_;
  private String type_;
  private String content_;
  private long order_;

  public class Type {
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String VIDEO = "video";
    public static final String PLACES = "places";
  }

  public static Element cursorToElement(Cursor cursor) {
    Element element = new Element();
    element.setId(cursor.getLong(0));
    element.setPostid(cursor.getLong(1));
    element.setType(cursor.getString(2));
    element.setContent(cursor.getString(3));
    element.setOrder(cursor.getLong(4));
    return element;
  }

  public long getId() {
    return id_;
  }

  public void setId(long id) {
    this.id_ = id;
  }

  public long getPostid() {
    return postid_;
  }

  public void setPostid(long postid) {
    this.postid_ = postid;
  }

  public String getType() {
    return type_;
  }

  public void setType(String type) {
    this.type_ = type;
  }

  public String getContent() {
    return content_;
  }

  public void setContent(String content) {
    this.content_ = content;
  }

  public long getOrder() {
    return order_;
  }

  public void setOrder(long order) {
    this.order_ = order;
  }

  @Override
  public int compareTo(Element another) {
    if (this.getOrder() == another.getOrder()) {
      return 0;
    }
    return this.getOrder() < another.getOrder() ? -1 : 1;
  }

}
