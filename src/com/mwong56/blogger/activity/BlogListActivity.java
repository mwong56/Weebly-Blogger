package com.mwong56.blogger.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;
import com.mwong56.blogger.R;
import com.mwong56.blogger.adapters.PostAdapter;
import com.mwong56.blogger.models.Post;
import com.mwong56.blogger.shared.PostsCallback;
import com.mwong56.blogger.shared.Utils;
import com.mwong56.blogger.sqlite.SQLDataSource;
import com.mwong56.blogger.views.PostView;

public class BlogListActivity extends ActionBarActivity {

  private ListView listView_;
  private SQLDataSource datasource_;
  private List<Post> posts_;
  private List<Post> filteredPosts_;
  private PostAdapter adapter_;
  private TextView noPostTextView_;
  private static final String TAG = "BlogListActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bloglist);
    setupReferences();
    setupFAB();
    datasource_ = SQLDataSource.getInstance(this);
    posts_ = new ArrayList<Post>();
    filteredPosts_ = new ArrayList<Post>();
    adapter_ = new PostAdapter(getApplicationContext(), filteredPosts_);
    listView_.setAdapter(adapter_);
    setupListeners();
  }

  private void setupListeners() {
    listView_.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PostView holder = (PostView) view.getTag();
        Post post = holder.post_;
        Intent i = new Intent(BlogListActivity.this, CrudBlogActivity.class);
        i.putExtra(Utils.ACTION, Utils.Status.EDIT);
        i.putExtra(Utils.ID, post.getId());
        startActivity(i);
      }
    });

    listView_.setOnItemLongClickListener(new OnItemLongClickListener() {

      @Override
      public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        PostView holder = (PostView) view.getTag();
        final Post post = holder.post_;
        AlertDialog.Builder builder = new AlertDialog.Builder(BlogListActivity.this);
        builder.setItems(new String[] { "Delete" }, new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            datasource_.deletePost(post);
            posts_.remove(post);
            filteredPosts_.remove(post);
            if (filteredPosts_.size() == 0) {
              noPostTextView_.setVisibility(View.VISIBLE);
            } else {
              noPostTextView_.setVisibility(View.GONE);
            }
            adapter_.notifyDataSetChanged();
          }
        });
        builder.show();
        return true;
      }
    });
  }

  private void getPost() {
    Log.i(TAG, "Getting posts");
    datasource_.getAllPosts(new PostsCallback() {

      @Override
      public void callback(List<Post> posts) {
        posts_.clear();
        filteredPosts_.clear();
        for (Post post : posts) {
          posts_.add(post);
        }
        filteredPosts_.addAll(posts_);
        if (filteredPosts_.size() == 0) {
          noPostTextView_.setVisibility(View.VISIBLE);
        } else {
          noPostTextView_.setVisibility(View.GONE);
        }
        if (adapter_ != null) {
          adapter_.notifyDataSetChanged();
        }
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    getPost();
  }

  private void setupReferences() {
    listView_ = (ListView) findViewById(R.id.list_view_blog);
    noPostTextView_ = (TextView) findViewById(R.id.noPostTextView);
  }

  private void setupFAB() {
    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.attachToListView(listView_);
    fab.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent i = new Intent(getApplicationContext(), CrudBlogActivity.class);
        i.putExtra(Utils.ACTION, Utils.Status.NEW);
        startActivity(i);
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    MenuItem searchItem = menu.findItem(R.id.action_search);
    SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
    searchView.setQueryHint("Search for a post");
    searchView.setOnQueryTextListener(new OnQueryTextListener() {

      @Override
      public boolean onQueryTextSubmit(String arg0) {
        return false;
      }

      @Override
      public boolean onQueryTextChange(String arg0) {
        String key = arg0.toLowerCase(Locale.ENGLISH).trim();
        filteredPosts_.clear();
        for (Post post : posts_) {
          if (post.getTitle().toLowerCase(Locale.ENGLISH).trim().contains(key)) {
            filteredPosts_.add(post);
          }
        }
        adapter_.notifyDataSetChanged();
        return true;
      }
    });
    return true;
  }
}
