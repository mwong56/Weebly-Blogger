package com.mwong56.blogger.adapters;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mwong56.blogger.R;
import com.mwong56.blogger.models.Element;
import com.mwong56.blogger.shared.Utils;
import com.mwong56.blogger.sqlite.SQLDataSource;
import com.mwong56.blogger.views.ElementView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ElementAdapter extends BaseAdapter {

  private Context context_;
  private List<Element> elementList_;

  private static final String TAG = ElementAdapter.class.getSimpleName();

  public ElementAdapter(Context context, List<Element> elementList) {
    this.context_ = context;
    this.elementList_ = elementList;
  }

  @Override
  public int getCount() {
    return elementList_.size();
  }

  @Override
  public Object getItem(int position) {
    return elementList_.get(position);
  }

  @Override
  public long getItemId(int position) {
    return elementList_.indexOf(elementList_.get(position));
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View view = convertView;
    final ElementView holder;

    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context_
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      view = inflater.inflate(R.layout.item_element, parent, false);
      holder = new ElementView();
      holder.text_ = (TextView) view.findViewById(R.id.text_view_element);
      holder.image_ = (ImageView) view.findViewById(R.id.image_view_element);
      view.setTag(holder);
    } else {
      holder = (ElementView) view.getTag();
    }

    Element element = elementList_.get(position);
    holder.element_ = element;
    String type = element.getType();
    String content = element.getContent();

    if (type.equals(Element.Type.TEXT)) {
      holder.text_.setVisibility(View.VISIBLE);
      holder.text_.setText(content);
    } else {
      holder.text_.setVisibility(View.GONE);
    }

    if (type.equals(Element.Type.IMAGE) || type.equals(Element.Type.VIDEO)
        || type.equals(Element.Type.PLACES)) {
      holder.image_.setVisibility(View.VISIBLE);
      if (type.equals(Element.Type.IMAGE)) {
        ImageLoader.getInstance().displayImage(content, holder.image_);
      }

      if (type.equals(Element.Type.VIDEO)) {
        Uri uri = Uri.parse(content);
        String filePath = Utils.getFilePath(context_, uri);
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
            MediaStore.Images.Thumbnails.MINI_KIND);
        holder.image_.setImageBitmap(thumbnail);
      }

      if (type.equals(Element.Type.PLACES)) {
        try {
          JSONObject json = new JSONObject(content);
          String lat = json.getString("lat");
          String lon = json.getString("long");
          String map = Utils.getGoogleMapUrl(Double.parseDouble(lat), Double.parseDouble(lon));
          ImageLoader.getInstance().displayImage(map, holder.image_);
        } catch (JSONException e) {
          Log.w(TAG, "Error parsing json: " + e.toString());
        }

      }

    } else {
      holder.image_.setVisibility(View.GONE);
    }

    return view;
  }

  public void remove(Element element, Boolean permanently) {
    elementList_.remove(element);
    if (permanently) {
      SQLDataSource.getInstance(context_).deleteElement(element);
    }
    notifyDataSetChanged();
  }

  public void insert(Element element, int index) {
    elementList_.add(index, element);
    notifyDataSetChanged();
  }

}