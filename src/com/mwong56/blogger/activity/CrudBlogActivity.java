package com.mwong56.blogger.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.text.format.Time;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocosw.undobar.UndoBarController.UndoBar;
import com.cocosw.undobar.UndoBarController.UndoListener;
import com.mobeta.android.dslv.DragSortController;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;
import com.mobeta.android.dslv.DragSortListView.RemoveListener;
import com.mwong56.blogger.R;
import com.mwong56.blogger.adapters.ElementAdapter;
import com.mwong56.blogger.models.Element;
import com.mwong56.blogger.models.Post;
import com.mwong56.blogger.shared.PostsCallback;
import com.mwong56.blogger.shared.Utils;
import com.mwong56.blogger.shared.Utils.Status;
import com.mwong56.blogger.sqlite.SQLDataSource;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CrudBlogActivity extends ActionBarActivity {
  private EditText numDay_;
  private EditText nameDay_;
  private EditText monthYear_;
  private EditText timeEditText_;
  private EditText titleEditText_;
  private Post post_;
  private ImageButton text_;
  private ImageButton image_;
  private ImageButton video_;
  private ImageButton places_;
  private SQLDataSource datasource_;
  private Status status_;
  private DragSortListView listView_;
  private ElementAdapter adapter_;
  private List<Element> elements_;
  private Uri outputFileUri_;
  private TextView noElementTextView_;

  private static final int SELECT_PICTURE = 1;
  private static final int SELECT_VIDEO = 2;
  private static final String TAG = CrudBlogActivity.class.getSimpleName();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_crud_blog);
    getSupportActionBar().setHomeButtonEnabled(true);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    elements_ = new ArrayList<Element>();
    datasource_ = SQLDataSource.getInstance(this);

    setupReferences();
    setupListeners();

    adapter_ = new ElementAdapter(getApplicationContext(), elements_);
    listView_.setAdapter(adapter_);
    setupDragController();

    status_ = (Status) getIntent().getExtras().get(Utils.ACTION);
    if (status_ == Status.NEW) {
      setupNewPost();
    } else if (status_ == Status.EDIT) {
      setupEditPost();
    }
  }

  private void setupNewPost() {
    getSupportActionBar().setTitle(R.string.new_post);
    setDateAndTime();
    post_ = datasource_.createPost(numDay_.getText().toString(), nameDay_.getText().toString(),
        monthYear_.getText().toString(), timeEditText_.getText().toString(), null, null, null);
    noElementTextView_.setVisibility(View.VISIBLE);
  }

  private void setupEditPost() {
    getSupportActionBar().setTitle(R.string.edit_post);
    datasource_.readPost(getIntent().getExtras().getLong(Utils.ID), new PostsCallback() {
      @Override
      public void callback(Post post) {
        post_ = post;
        for (Element element : post.getElements()) {
          elements_.add(element);
        }
        Collections.sort(elements_);
        if (post.getElements().size() == 0) {
          noElementTextView_.setVisibility(View.VISIBLE);
        } else {
          noElementTextView_.setVisibility(View.GONE);
        }
        adapter_.notifyDataSetChanged();
        titleEditText_.setText(post_.getTitle());
        setDateAndTime();
      }
    });
  }

  private void setupDragController() {
    DragSortController controller = new DragSortController(listView_);
    controller.setDragHandleId(R.id.drag_handle);
    controller.setRemoveEnabled(true);
    controller.setSortEnabled(true);
    controller.setDragInitMode(DragSortController.ON_LONG_PRESS);
    listView_.setFloatViewManager(controller);
    listView_.setOnTouchListener(controller);
    listView_.setDragEnabled(true);
  }

  private void setupReferences() {
    listView_ = (DragSortListView) findViewById(R.id.crud_list_view);
    text_ = (ImageButton) findViewById(R.id.text_button);
    image_ = (ImageButton) findViewById(R.id.photo_button);
    video_ = (ImageButton) findViewById(R.id.video_button);
    places_ = (ImageButton) findViewById(R.id.places_button);
    numDay_ = (EditText) findViewById(R.id.numDay);
    nameDay_ = (EditText) findViewById(R.id.nameDay);
    monthYear_ = (EditText) findViewById(R.id.monthYear);
    timeEditText_ = (EditText) findViewById(R.id.time);
    titleEditText_ = (EditText) findViewById(R.id.title);
    noElementTextView_ = (TextView) findViewById(R.id.noElementTextView);
  }

  private void setupListeners() {
    text_.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        showTextDialog();
      }
    });

    image_.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        openImageIntent();
      }
    });

    video_.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        openVideoIntent();
      }
    });

    places_.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        showPlacesDialog();
      }
    });

    listView_.setDropListener(new DropListener() {

      @Override
      public void drop(int from, int to) {
        if (from != to) {
          Element element = (Element) adapter_.getItem(from);
          adapter_.remove(element, false);
          adapter_.insert(element, to);
        }
      }
    });

    listView_.setRemoveListener(new RemoveListener() {

      @Override
      public void remove(int which) {
        final int index = which;
        final Element elementToRemove = (Element) adapter_.getItem(which);
        adapter_.remove(elementToRemove, true);
        if (post_.getElements().size() == 0) {
          noElementTextView_.setVisibility(View.VISIBLE);
        } else {
          noElementTextView_.setVisibility(View.GONE);
        }
        new UndoBar(CrudBlogActivity.this).message("Deleted").listener(new UndoListener() {

          @Override
          public void onUndo(Parcelable token) {
            Element reinsertElement = SQLDataSource.getInstance(getApplicationContext())
                .createElement(elementToRemove.getPostid(), elementToRemove.getType(),
                    elementToRemove.getContent(), index);
            adapter_.insert(reinsertElement, index);
            noElementTextView_.setVisibility(View.GONE);
          }
        }).show();
      }
    });
    listView_.setOnItemClickListener(new OnItemClickListener() {

      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Element element = elements_.get(position);
        if (element.getType().equals(Element.Type.TEXT)
            || element.getType().equals(Element.Type.PLACES)) {
          return;
        }

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        if (element.getType().equals(Element.Type.IMAGE)) {
          Uri uri = Uri.parse(element.getContent());
          intent.setDataAndType(uri, "image/*");
        } else if (element.getType().equals(Element.Type.VIDEO)) {
          Uri uri = Uri.parse(element.getContent());
          intent.setDataAndType(uri, "video/*");
        }

        startActivity(intent);
      }
    });
  }

  /*
   * Saving the post into the database. If post title is not set, we set it to
   * Post #<id> Here we also set the order of the elements.
   */
  @Override
  protected void onPause() {
    super.onPause();
    // Set title
    if (titleEditText_.getText().toString().length() > 0) {
      post_.setTitle(titleEditText_.getText().toString());
    } else {
      post_.setTitle("Post #" + post_.getId());
    }

    // Clear post text and image.
    post_.setPost(null);
    post_.setImage(null);
    boolean isPostSet = false;
    boolean isImageSet = false;

    for (int i = 0; i < elements_.size(); i++) {
      Element element = elements_.get(i);
      element.setOrder(i);
      if (!isPostSet && element.getType().equals(Element.Type.TEXT)) {
        post_.setPost(element.getContent());
        isPostSet = true;
      } else if (!isImageSet && element.getType().equals(Element.Type.IMAGE)) {
        post_.setImage(element.getContent());
        isImageSet = true;
      }
      datasource_.updateElement(element);
    }
    datasource_.updatePost(post_);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case android.R.id.home:
      Intent i = new Intent(getApplicationContext(), BlogListActivity.class);
      startActivity(i);
      overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
      return true;
    }
    return false;
  }

  private void setDateAndTime() {
    if (status_ == Status.NEW) {
      Time today = new Time(Time.getCurrentTimezone());
      today.setToNow();
      String month = Calendar.getInstance()
          .getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
          .toUpperCase(Locale.ENGLISH);
      String nameDay = Calendar.getInstance().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG,
          Locale.getDefault());
      numDay_.setText(today.monthDay + "");
      nameDay_.setText(nameDay + "");
      monthYear_.setText(month + " " + today.year);

      String ampm = (Calendar.getInstance().get(Calendar.AM_PM) == Calendar.AM) ? "AM" : "PM";
      timeEditText_.setText(today.format("%k:%M") + " " + ampm);
    } else {
      numDay_.setText(post_.getNumday());
      nameDay_.setText(post_.getNameday());
      monthYear_.setText(post_.getMonthyear());
      timeEditText_.setText(post_.getTime());
    }
  }

  private void openImageIntent() {
    // Determine Uri of camera image to save.
    final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir"
        + File.separator);
    root.mkdirs();
    final String fname = Utils.getUniqueImageFilename();
    final File sdImageMainDirectory = new File(root, fname);
    outputFileUri_ = Uri.fromFile(sdImageMainDirectory);

    // Camera.
    final List<Intent> cameraIntents = new ArrayList<Intent>();
    final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
    final PackageManager packageManager = getPackageManager();
    final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
    for (ResolveInfo res : listCam) {
      final String packageName = res.activityInfo.packageName;
      final Intent intent = new Intent(captureIntent);
      intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
      intent.setPackage(packageName);
      intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri_);
      cameraIntents.add(intent);
    }

    // Filesystem.
    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

    // Chooser of filesystem options.
    final Intent chooserIntent = Intent.createChooser(pickIntent, "Select Source");

    // Add the camera options.
    chooserIntent
        .putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[] {}));

    startActivityForResult(chooserIntent, SELECT_PICTURE);
  }

  private void openVideoIntent() {
    // Determine Uri of video image to save.
    final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir"
        + File.separator);
    root.mkdirs();
    final String fname = Utils.getUniqueVideoFilename();
    final File sdImageMainDirectory = new File(root, fname);
    outputFileUri_ = Uri.fromFile(sdImageMainDirectory);

    // Video.
    final List<Intent> videoIntents = new ArrayList<Intent>();
    final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
    final PackageManager packageManager = getPackageManager();
    final List<ResolveInfo> listVid = packageManager.queryIntentActivities(captureIntent, 0);
    for (ResolveInfo res : listVid) {
      final String packageName = res.activityInfo.packageName;
      final Intent intent = new Intent(captureIntent);
      intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
      intent.setPackage(packageName);
      intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri_);
      videoIntents.add(intent);
    }

    // Filesystem.
    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

    // Chooser of filesystem options.
    final Intent chooserIntent = Intent.createChooser(pickIntent, "Select Source");

    // Add the camera options.
    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, videoIntents.toArray(new Parcelable[] {}));

    startActivityForResult(chooserIntent, SELECT_VIDEO);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      if (requestCode == SELECT_PICTURE) {
        final boolean isCamera;
        if (data == null) {
          isCamera = true;
        } else {
          final String action = data.getAction();
          if (action == null) {
            isCamera = false;
          } else {
            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
          }
        }

        Uri selectedImageUri;
        if (isCamera) {
          selectedImageUri = outputFileUri_;
        } else {
          selectedImageUri = data == null ? null : data.getData();
        }
        Log.d(TAG, selectedImageUri.toString());
        showImageDialog(selectedImageUri);
      } else if (requestCode == SELECT_VIDEO) {
        Uri selectedVideoUri = data.getData();
        Log.d(TAG, "SelectedVideoUri = " + selectedVideoUri);
        String filePath = Utils.getFilePath(getApplicationContext(), selectedVideoUri);
        showVideoDialog(selectedVideoUri, filePath);
      }
    }
  }

  private void showTextDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
        CrudBlogActivity.this, R.style.AlertDialogCustom));
    View dialogView = getLayoutInflater().inflate(R.layout.text_dialog, null);
    builder.setView(dialogView);

    TextView textView = (TextView) dialogView.findViewById(R.id.title_view);
    textView.setText("New Text");

    final EditText editText = (EditText) dialogView.findViewById(R.id.edit_text_view);
    builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        Element element = datasource_.createElement(post_.getId(), Element.Type.TEXT, editText
            .getText().toString(), elements_.size());
        if (post_.getPost() == null) {
          post_.setPost(editText.getText().toString());
        }
        elements_.add(element);
        noElementTextView_.setVisibility(View.GONE);
        adapter_.notifyDataSetChanged();
      }
    });

    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        return;
      }
    });

    builder.show();
  }

  private void showImageDialog(final Uri uri) {
    if (uri == null) {
      return;
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
        CrudBlogActivity.this, R.style.AlertDialogCustom));
    View dialogView = getLayoutInflater().inflate(R.layout.image_dialog, null);
    builder.setView(dialogView);

    TextView textView = (TextView) dialogView.findViewById(R.id.title_view);
    textView.setText("New Image");
    ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_view);
    imageView.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
      }
    });

    ImageLoader.getInstance().displayImage(uri.toString(), imageView);
    builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        Element element = datasource_.createElement(post_.getId(), Element.Type.IMAGE,
            uri.toString(), elements_.size());
        if (post_.getImage() == null) {
          post_.setImage(uri.toString());
        }
        elements_.add(element);
        noElementTextView_.setVisibility(View.GONE);
        adapter_.notifyDataSetChanged();
      }
    });

    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        return;
      }
    });

    builder.show();
  }

  private void showVideoDialog(final Uri uri, final String filePath) {
    if (uri == null) {
      return;
    }

    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
        CrudBlogActivity.this, R.style.AlertDialogCustom));
    View dialogView = getLayoutInflater().inflate(R.layout.image_dialog, null);
    builder.setView(dialogView);

    TextView textView = (TextView) dialogView.findViewById(R.id.title_view);
    textView.setText("New Video");
    ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_view);
    imageView.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "video/*");
        startActivity(intent);
      }
    });

    Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(filePath,
        MediaStore.Images.Thumbnails.MINI_KIND);
    imageView.setImageBitmap(thumbnail);

    builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        Element element = datasource_.createElement(post_.getId(), Element.Type.VIDEO,
            uri.toString(), elements_.size() - 1);
        elements_.add(element);
        noElementTextView_.setVisibility(View.GONE);
        adapter_.notifyDataSetChanged();
      }
    });

    builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        return;
      }
    });

    builder.show();
  }

  private void showPlacesDialog() {
    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(
        CrudBlogActivity.this, R.style.AlertDialogCustom));
    View dialogView = getLayoutInflater().inflate(R.layout.places_dialog, null);
    builder.setView(dialogView);

    TextView textView = (TextView) dialogView.findViewById(R.id.title_view);
    textView.setText("New Text");

    final EditText latText = (EditText) dialogView.findViewById(R.id.lat_edit_view);
    latText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
    final EditText lonText = (EditText) dialogView.findViewById(R.id.long_edit_view);
    lonText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);

    builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {

      @Override
      public void onClick(DialogInterface dialog, int which) {
        JSONObject json = new JSONObject();
        try {
          Double lat = Double.parseDouble(latText.getText().toString());
          Double lon = Double.parseDouble(lonText.getText().toString());
          json.put("lat", lat);
          json.put("long", lon);
        } catch (Exception e) {
          Log.w(TAG, "Error creating json object for places element. Error = " + e.toString());
          return;
        }
        Element element = datasource_.createElement(post_.getId(), Element.Type.PLACES,
            json.toString(), elements_.size());
        elements_.add(element);
        noElementTextView_.setVisibility(View.GONE);
        adapter_.notifyDataSetChanged();
      }
    });

    builder.show();

  }
}
